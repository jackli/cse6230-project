#!/bin/bash

for i in {2..13}
do
  ssh node-$i "
    sudo rpm -Uvh ~/epel-release-5*.rpm
    sudo yum -y install boost-devel.x86_64 boost-devel.i386 cmake28.x86_64 gcc44.x86_64
    sudo ln -s /usr/bin/cmake28 /usr/bin/cmake
    cd /tmp; tar xvzf ~/metis-5.1.0.tar.gz; cd metis*; make config cc=/usr/bin/gcc44; make cc=/usr/bin/gcc44; sudo make install
  " &
done
