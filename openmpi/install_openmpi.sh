#!/bin/bash

for i in {2..13}
do
  ssh node-$i "
    sudo apt-get -y install build-essential g++ python-dev autotools-dev libicu-dev libbz2-dev
    sudo apt-get -y install openmpi-bin openmpi-common libopenmpi1.3 libopenmpi-dev libboost1.48-all-dev cmake sysstat
  " &
done
