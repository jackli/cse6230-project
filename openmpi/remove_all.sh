#!/bin/bash

for i in {1..14}
do
  ssh node-$i "
    sudo killall java
    rm -rf /mnt/*
    sudo rm /bin/java
    sudo rm -rf /var/data/hadoop
  " &
done
