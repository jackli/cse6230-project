#!/bin/bash

for i in {2..13}
do
  ssh node-$i "
    sudo rpm -Uvh ~/epel-release-5*.rpm
    sudo yum -y install boost-devel.x86_64 boost-devel.i386 cmake28.x86_64 gcc44.x86_64 boost141*.x86_64 
  " &
done
