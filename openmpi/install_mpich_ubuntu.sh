#!/bin/bash

for i in {2..13}
do
  ssh node-$i "
    sudo apt-get remove mpich2 libmpich2-dev -y
  " &
done
