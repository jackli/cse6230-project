#!/bin/bash

for i in {2..13}
do
  ssh node-$i "
    cd /tmp
    tar xvzf ~/mpich*.tar.gz
    cd mpich*
    ./configure --disable-f77 --disable-f90 --disable-fortran
    make
    sudo make install
  " &
done
