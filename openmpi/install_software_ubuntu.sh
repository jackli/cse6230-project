#!/bin/bash

for i in {2..13}
do
  ssh node-$i "
    cd /tmp; tar xvzf ~/boost*.tar.gz; cd boost*; ./bootstrap.sh --prefix=/usr/local; sudo ./b2
  " &
done
