#!/bin/bash

HOME=/users/jyl
SOFTWARE_DIR=$HOME/software
DATA_DIR=/var/data/hadoop/hdfs
CONFIG_DIR=$HOME/hadoop_config
HADOOP_TARBALL=hadoop-2.6.0.tar.gz

# install hadoop
INSTALL_HADOOP="sudo chmod 777 /mnt; tar xvzf $SOFTWARE_DIR/$HADOOP_TARBALL --directory=/mnt; mv /mnt/hadoop-2.6.0 /mnt/hadoop; cp $CONFIG_DIR/*.xml /mnt/hadoop/etc/hadoop/; cp $CONFIG_DIR/hadoop-env.sh /mnt/hadoop/etc/hadoop/; cp $CONFIG_DIR/slaves /mnt/hadoop/etc/hadoop/"
# install java
# RHEL
#INSTALL_JAVA="tar xvzf $SOFTWARE_DIR/jdk-*.tar.gz --directory=/mnt; sudo alternatives --install /usr/bin/java java /mnt/jdk1.7.0_51/bin/java 1; echo -e \"4\n\" | sudo alternatives --config java"
# Ubuntu
INSTALL_JAVA="tar xvzf $SOFTWARE_DIR/jdk-*.tar.gz --directory=/mnt; sudo update-alternatives --install /usr/bin/java java /mnt/jdk1.7.0_51/bin/java 1; echo -e \"1\n\" | sudo update-alternatives --config java"
# add users
#INSTALL_CMD="$INSTALL_CMD; sudo groupadd hadoop; sudo useradd -g hadoop yarn; sudo useradd -g hadoop hdfs; sudo useradd -g hadoop mapred"
# add directories
MK_DATA_DIR="sudo mkdir -p $DATA_DIR/nn; sudo mkdir -p $DATA_DIR/snn; sudo mkdir -p $DATA_DIR/dn; sudo chmod -R 777 $DATA_DIR; sudo chown lijack -R $DATA_DIR"

INSTALL_CMD="$MK_DATA_DIR; $INSTALL_HADOOP; $INSTALL_JAVA"

for i in `seq 1 8`;
do
	ssh node-$i "$INSTALL_CMD" &
done
