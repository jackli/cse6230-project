#!/bin/bash

export HOME=/users/jyl
export SOFTWARE_DIR=$HOME/software
export DATA_DIR=/var/data/hadoop/hdfs
export SPARK_CONFIG_DIR=$HOME/spark_config
export SPARK_DIR="/mnt/spark-1.1.1"

# install scala
INSTALL_SCALA="tar xvzf $SOFTWARE_DIR/scala*.tgz --directory=/mnt"
# install spark
INSTALL_SPARK="tar xvzf $SOFTWARE_DIR/spark-1.1.1.tgz --directory=/mnt; mv /mnt/spark-*-bin-*/ $SPARK_DIR; cp $SPARK_CONFIG_DIR/* $SPARK_DIR/conf"
# set misc variables
SET_VAR="sudo ln -s /usr/bin/java /bin/java"

INSTALL_CMD="$INSTALL_SCALA; $INSTALL_SCALA; $INSTALL_SPARK; $SET_VAR"

for i in `seq 1 8`;
do
	ssh node-$i "$INSTALL_CMD" &
done

sleep 100
