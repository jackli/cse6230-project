import org.apache.spark._
import org.apache.spark.graphx._
import org.apache.spark.rdd.RDD

object PageRank {
  def main(args: Array[String]) {
    // Load the edges as a graph
    val graph = GraphLoader.edgeListFile(sc, "gplus.graph")
    // Run PageRank
    val ranks = graph.pageRank(0.0000001).vertices
    // Join the ranks with the usernames
    val users = sc.textFile("gplus.graph.followers").map { line =>
      val fields = line.split(",")
      (fields(0).toLong, fields(1))
    }
    val ranksByUsername = users.join(ranks).map {
      case (id, (username, rank)) => (username, rank)
    }
    // Print the result
    println(ranksByUsername.collect().mkString("\n"))
  }
}
