#include <iostream>
#include <fstream>
#include <omp.h>
#include <vector>
#include <windows.h>
#include <ctime>
#include <deque>

#define TOP_DOWN 0
#define BOTTOM_UP 1

using   namespace   std;

typedef vector<int> VI;
typedef vector<VI> VII;

VII     G;
int     Source_Node;
int     NNode;
int     NEdge;
bool*   visited;

int     mf; //The number of edges to check from the frontier
int     mu; //The number of edges to check from unexplored vertices
int     nf; //The number of vertices in the frontier
double  alpha = 14, beta = 20; //Hybrid parameter

void    input_graph()
{
    scanf ("%d%d", &NNode, &NEdge);
    for (int i = 0; i < NNode; i ++)
    {
        VI tmp; tmp.clear();
        int tot;
        scanf("%d", &tot);
        for (int j = 0; j < tot; j ++)
        {
            int tmp2;
            scanf("%d", &tmp2);
            tmp.push_back(tmp2);
        }
        //cout << i << " " << tmp.size() << endl;
        G.push_back(tmp);
    }
    scanf("%d", &Source_Node);

    visited = (bool*) malloc(sizeof(bool) * NNode);

    for (int i = 0; i < NNode; i ++) visited[i] = false;
    mu = NEdge * 2;
    cout << "Input Done" << endl;
}

double    bfs()
{
    Source_Node = 0;

    cout << "Start BFS" << endl;
	LARGE_INTEGER Frequency;
    LARGE_INTEGER start_PerformanceCount;
    LARGE_INTEGER end_PerformanceCount;
    double run_time;
    QueryPerformanceFrequency(&Frequency);

	QueryPerformanceCounter(&start_PerformanceCount);

    int     currentChoice;

	 deque<int> current,next;
	 next.clear();

    omp_set_num_threads(1);

	 visited[Source_Node] = true;
	 current.push_back(Source_Node);

     mf = G[Source_Node].size();
     mu -= mf;

     //cout << mu << endl;

     currentChoice = TOP_DOWN;

	 while(!current.empty())
        {
            //cout << mu << " " << (double)mu/alpha << " " << nf << " " << (double)NNode/beta << endl;
            if ((currentChoice == TOP_DOWN) && (mf > (double)mu/alpha)) {currentChoice = BOTTOM_UP; cout << "Switch to Bottom-up" << endl;}
                else if ((currentChoice == BOTTOM_UP) && (nf < (double)NNode/beta)) {currentChoice = TOP_DOWN; cout << "Switch to Top-down" << endl;}
            mf = 0; nf = 0;
            if (currentChoice == TOP_DOWN) {
                int parallel_num = current.size();
#pragma omp parallel for
                for (int j=0; j<parallel_num; j++) {
                    int index;
#pragma omp critical
               {
                    index = current.front();
                    current.pop_front();
			   }
               int  Neighbor_Size = G[index].size();
			   for (int i = 0; i < Neighbor_Size; i ++)
               {
					int     id = G[index][i];
                    if (!visited[id]) {
                         bool its_color = __sync_lock_test_and_set(&visited[id], true);
                         if (its_color == false) {
#pragma omp critical
                              {
                                   next.push_back(id);
                                   mf += G[id].size();
                                   //cout << id << " " << mf << endl;
                              }
                         }
					}
			   }
               }
               mu -= mf;
               nf = next.size();
			   current.swap(next);
               next.clear();
		  }//Top-down BFS end
        else
        {
        int parallel_num = NNode;
#pragma omp parallel for
		  for (int j=0; j<parallel_num; j++)
            if (!visited[j]) {
               int  index = j;
               int  Neighbor_Size = G[index].size();
			   for (int i = 0; i < Neighbor_Size; i ++)
               {
					int     id = G[index][i];
                    if (visited[id]) {
                         bool its_color = __sync_lock_test_and_set(&visited[j], true);
                         if (its_color == false) {
#pragma omp critical
                              {
                                   next.push_back(j);
                                   mf += G[j].size();
                              }
                         }
					}
			   }
		  }
               mu -= mf;
               nf = next.size();
			   current.swap(next);
               next.clear();
        }
	 }

    for (int i = 0; i < NNode; i ++) if (!visited[i]) cout << "Node:" << i << endl;

    //cout << mu << endl;
	QueryPerformanceCounter(&end_PerformanceCount);
	run_time = (end_PerformanceCount.QuadPart - start_PerformanceCount.QuadPart) / (double)Frequency.QuadPart;
	return run_time;
}

int     main()
{
    freopen ("SimpleGraph.txt", "r", stdin);
    input_graph();
    cout << bfs() << endl;
}
