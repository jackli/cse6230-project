#include <iostream>
#include <fstream>
#include <vector>
#include <deque>
#include <windows.h>
#include <ctime>
using   namespace   std;

typedef vector<int> VI;
typedef vector<VI> VII;

VII     G;
int     Source_Node;
int     NNode;
int     NEdge;
bool*   visited;


void    input_graph()
{
    scanf ("%d%d", &NNode, &NEdge);
    cout << NNode << " " << NEdge << endl;
    for (int i = 0; i < NNode; i ++)
    {
        VI tmp; tmp.clear();
        int tot;
        scanf("%d", &tot);
        for (int j = 0; j < tot; j ++)
        {
            int tmp2;
            scanf("%d", &tmp2);
            tmp.push_back(tmp2);
        }
        G.push_back(tmp);
    }
    scanf("%d", &Source_Node);

    visited = (bool*) malloc(sizeof(bool) * NNode);

    memset (visited, 0, sizeof (visited));

    cout << "Input Done" << endl;
}

double  bfs()
{
    Source_Node = 0;
    //cout << "Start BFS" << endl;
	LARGE_INTEGER Frequency;
    LARGE_INTEGER start_PerformanceCount;
    LARGE_INTEGER end_PerformanceCount;
    double run_time;
    QueryPerformanceFrequency(&Frequency);

	QueryPerformanceCounter(&start_PerformanceCount);

    VI Q;
    Q.clear(); Q.push_back(Source_Node);

	 visited[Source_Node] = true;

	 for (int first = 0; first < Q.size(); first ++)
     {
         int    cur = Q[first];
         int  Neighbor_Size = G[cur].size();
			   for (int i = 0; i < Neighbor_Size; i ++)
               {
					int     id = G[cur][i];
                    if (!visited[id]) {
                        Q.push_back(id);
                        visited[id] = true;
                    }
               }
    }

    cout << Q.size() << " " << NNode << endl;
	QueryPerformanceCounter(&end_PerformanceCount);
	run_time = (end_PerformanceCount.QuadPart - start_PerformanceCount.QuadPart) / (double)Frequency.QuadPart;
	return run_time;
}

int     main()
{
    freopen ("SimpleGraph.txt", "r", stdin);
    input_graph();
    cout << bfs() << endl;
}
