#include <iostream>
#include <fstream>
#include <omp.h>
#include <vector>
#include <windows.h>
#include <ctime>
#include <deque>

using   namespace   std;

typedef vector<int> VI;
typedef vector<VI> VII;

VII     G;
int     Source_Node;
int     NNode;
int     NEdge;
bool*   visited;


void    input_graph()
{
    scanf ("%d%d", &NNode, &NEdge);
    for (int i = 0; i < NNode; i ++)
    {
        VI tmp; tmp.clear();
        int tot;
        scanf("%d", &tot);
        for (int j = 0; j < tot; j ++)
        {
            int tmp2;
            scanf("%d", &tmp2);
            tmp.push_back(tmp2);
        }
        G.push_back(tmp);
    }
    scanf("%d", &Source_Node);

    visited = (bool*) malloc(sizeof(bool) * NNode);

    for (int i = 0; i < NNode; i ++) visited[i] = false;

    cout << "Input Done" << endl;
}

double    bfs()
{
    Source_Node = 0;

    cout << "Start BFS" << endl;
	LARGE_INTEGER Frequency;
    LARGE_INTEGER start_PerformanceCount;
    LARGE_INTEGER end_PerformanceCount;
    double run_time;
    QueryPerformanceFrequency(&Frequency);

	QueryPerformanceCounter(&start_PerformanceCount);

	 deque<int> current[2];
    current[0].clear(); current[1].clear();

    omp_set_num_threads(16);

    int     now = 0, next = 1;

	 visited[Source_Node] = true;
	 current[now].push_back(Source_Node);

    int     totnode = 0;

	 while(!current[now].empty()) {
        totnode += current[now].size();
          int parallel_num = current[now].size();
#pragma omp parallel for
		  for (int j=0; j<parallel_num; j++) {
               unsigned int index;
#pragma omp critical
               {
                    index = current[now].front();
                    current[now].pop_front();
			   }
               int  Neighbor_Size = G[index].size();
			   for (int i = 0; i < Neighbor_Size; i ++)
               {
					int     id = G[index][i];
                    if (!visited[id]) {
                         bool its_color = __sync_lock_test_and_set(&visited[id], true);
                         if (its_color == false) {
                              //cost[id] = cost[index] + 1;
#pragma omp critical
                              {
                                   current[next].push_back(id);
                              }
                         }
					}
			   }
		  }
		  now = 1 - now; next = 1 - next;
	 }
    cout << totnode << " " << NNode << endl;
	QueryPerformanceCounter(&end_PerformanceCount);
	run_time = (end_PerformanceCount.QuadPart - start_PerformanceCount.QuadPart) / (double)Frequency.QuadPart;
	return run_time;
}

int     main()
{
    freopen ("TestGraph1.in", "r", stdin);
    input_graph();
    cout << bfs() << endl;
}
