#include <iostream>
#include <fstream>
#include <omp.h>
#include <vector>
#include <windows.h>
#include <ctime>
#include <deque>
#include <tbb/concurrent_queue.h>

using   namespace   std;

typedef vector<int> VI;
typedef vector<VI> VII;

VII     G;
int     Source_Node;
int     NNode;
int     NEdge;
bool*   visited;


void    input_graph()
{
    scanf ("%d%d", &NNode, &NEdge);
    for (int i = 0; i < NNode; i ++)
    {
        VI tmp; tmp.clear();
        int tot;
        scanf("%d", &tot);
        for (int j = 0; j < tot; j ++)
        {
            int tmp2;
            scanf("%d", &tmp2);
            tmp.push_back(tmp2);
        }
        G.push_back(tmp);
    }
    scanf("%d", &Source_Node);

    visited = (bool*) malloc(sizeof(bool) * NNode);

    for (int i = 0; i < NNode; i ++) visited[i] = false;

    cout << "Input Done" << endl;
}

double    bfs()
{
    Source_Node = 0;

    cout << "Start BFS" << endl;
	LARGE_INTEGER Frequency;
    LARGE_INTEGER start_PerformanceCount;
    LARGE_INTEGER end_PerformanceCount;
    double run_time;
    QueryPerformanceFrequency(&Frequency);

	QueryPerformanceCounter(&start_PerformanceCount);

	 tbb::concurrent_bounded_queue<int> current,next;
	 next.clear();

    omp_set_num_threads(1);

	 visited[Source_Node] = true;
	 current.push_back(Source_Node);

	 while(!current.empty()) {
          int parallel_num = current.size();
#pragma omp parallel for
		  for (int j=0; j<parallel_num; j++) {
               int index;
               index = current.front();
               current.pop_front();

               int  Neighbor_Size = G[index].size();
			   for (int i = 0; i < Neighbor_Size; i ++)
               {
					int     id = G[index][i];
                    if (!visited[id]) {
                                    visited[id]=true;
                                   next.push_back(id);
                              }
			   }
		  }
		  current.swap(next);
          next.clear();
	 }

	QueryPerformanceCounter(&end_PerformanceCount);
	run_time = (end_PerformanceCount.QuadPart - start_PerformanceCount.QuadPart) / (double)Frequency.QuadPart;
	return run_time;
}

int     main()
{
    freopen ("SimpleGraph.txt", "r", stdin);
    input_graph();
    cout << bfs() << endl;
}
