#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>

using   namespace   std;

typedef vector<int> VI;

stringstream ss;
string      tmp;

int     main()
{
    freopen ("graph1.gr", "r", stdin);
    freopen ("SimpleGraph.txt", "w", stdout);

    int     n, m;
    cin >> n >> m;
    cout << n << " " << m << endl;
    getline(cin, tmp);
    for (int i = 0; i < n; i ++)
    {
        getline(cin, tmp);
        ss.clear();
        VI tt;
        tt.clear();
        ss << tmp << endl;
        int     p;
        while (ss >> p) tt.push_back(p - 1);
        cout << tt.size();
        for (int j = 0; j < tt.size(); j ++) cout << " " << tt[j];
        cout << endl;
    }
}
