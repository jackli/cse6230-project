#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>

using   namespace   std;

typedef vector<int> VI;
typedef vector<VI> VII;

stringstream ss;
string      tmp;
VI         G[200000];


int     main()
{
    freopen ("TestGraph1.txt", "r", stdin);
    freopen ("TestGraph1.in", "w", stdout);

    int     n, m;
    cin >> n >> m;
    cout << n << " " << m << endl;
    for (int i = 0; i < m; i ++)
    {
        int a, b;
        cin >> a >> b;
        G[a].push_back(b);
        G[b].push_back(a);
    }

    for (int i = 0; i < n; i ++) {
        cout << G[i].size();
        for (int j = 0; j < G[i].size(); j ++) cout << " " << G[i][j];
        cout << endl;
    }

    return 0;
}
